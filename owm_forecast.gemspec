# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'owm_forecast/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'owm_forecast'
  s.version     = OwmForecast::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/owm_forecast'
  s.summary     = 'Widget for mirr.OS that shows a 5-day-forecast from compatible sources.'
  s.description = 'Shows a five-day forecast from Openweathermap sources.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Forecast',
                      deDe: 'Vorhersage',
                      frFr: 'Prévoir',
                      esEs: 'Pronóstico',
                      plPl: 'Prognoza',
                      koKr: '예보'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeigt eine fünf-Tages-Vorhersage aus Openweathermap-Datenquellen.',
                      frFr: 'Affiche une prévision sur cinq jours à partir de sources Openweathermap.',
                      esEs: 'Muestra un pronóstico de cinco días de fuentes Openweathermap.',
                      plPl: 'Pokazuje pięciodniową prognozę ze źródeł Openweathermap.',
                      koKr: 'Openweathermap 소스에서 5 일 동안의 일기 예보를 보여줍니다.'
                    },
                    sizes: [
                      {
                        w: 3,
                        h: 3
                      },
                      {
                        w: 4,
                        h: 4
                      },
                      {
                        w: 6,
                        h: 5
                      },
                      {
                        w: 8,
                        h: 6
                      },
                      {
                        w: 10,
                        h: 8
                      },
                      {
                        w: 12,
                        h: 10
                      }
                    ],
                    languages: %i[enGb deDe frFr esEs plPl koKr],
                    group: 'weather_owm',
                    compatibility: '0.9.1',
                    single_source: true
                  }.to_json }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.81'
  s.add_development_dependency 'rubocop-rails'
end
