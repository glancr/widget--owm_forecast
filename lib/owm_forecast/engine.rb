# frozen_string_literal: true

module OwmForecast
  class Engine < ::Rails::Engine
    isolate_namespace OwmForecast
    config.generators.api_only = true
  end
end
