# frozen_string_literal: true

Rails.application.routes.draw do
  mount OwmForecast::Engine => '/owm_forecast'
end
