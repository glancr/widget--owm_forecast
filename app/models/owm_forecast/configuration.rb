# frozen_string_literal: true

module OwmForecast
  class Configuration < WidgetInstanceConfiguration
    attribute :show_location_name, :boolean, default: true

    validates :show_location_name, boolean: true
  end
end
